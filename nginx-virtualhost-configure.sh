#!/bin/bash
clear

# Show Welcome Message
stripe() { 
  ss=$(
  echo "$1" | sed -e 's:\(\\\(e\|033\|x1B\)\)\(\[\([0-9]\{1,3\};\?\)\+m\)::g'); 
  echo $ss; 
};

centerize() { 
  strip=$(stripe "$1"); 
  sps=$(( ($(tput cols) - ${#strip}) / 2 )); 
  printf "%*s%s%*s\n" $sps " " "$(echo -e $1)" $sps " "; 
};

centerize "Hi \e[0;31m$USER\e[0m,"
centerize "Script for Configure Virtualhost Nginx"
centerize "Enjoy That! ;)"
sleep 5; clear


# Make Directory on /var/www/$MKDC/html/
read -p "Type for Make Directory: (Ex: example.net) " MKDC
sudo mkdir -p /var/www/$MKDC/html/ 
echo -e "$MKDC Created!\n\n"


# Create Index.html on Directory
touch /var/www/$MKDC/html/index.html
echo "<html>
    <head>
        <title>Welcome to $MKDC!</title>
    </head>
    <body>
        <h1>Success!  The $MKDC server block is working!</h1>
    </body>
</html>" | tee /var/www/$MKDC/html/index.html > /dev/null
echo -e "index.html Created!\n\n"


# Chown Directory
read -p "Type Chown User: (Default: USER) " CUDC
	 if [[ -z $CUDC ]]; then
    	chown -R $USER:$USER /var/www/$MKDC/;
        echo -e "Chown $USER for $MKDC!\n\n"
    else
    	chown -R $CUDC:$CUDC /var/www/$MKDC/;
        echo -e "Chown $CUDC for $MKDC!\n\n"
	fi 


# Copy Nginx Config
sudo cp /etc/nginx/sites-available/default /etc/nginx/sites-available/$MKDC
echo -e "Copy Nginx Config!\n"

# Edit Virtualhost Config
echo "server {
        listen  80;
        listen [::]:80;
 
        root /var/www/$MKDC/html;
        index index.html index.htm index.nginx-debian.html;
 
        server_name $MKDC www.$MKDC;
 
        location / {
                try_files \$uri \$uri/ =404;
        }
}" | sudo tee /etc/nginx/sites-available/$MKDC > /dev/null
echo -e "Edit Virtualhost Config!\n"

# Shortcut for Nginxginx Config
sudo ln -s /etc/nginx/sites-available/$MKDC /etc/nginx/sites-enabled/
echo -e "Shortcut Created!\n\n"


# Edit Hosts
read -p "Type Server Address: (Default: 138.201.73.190) " SVEH
	if [[ -z $SVEH ]]; then
    	echo "138.201.73.190 $MKDC www.$MKDC" | sudo tee -a /etc/hosts > /dev/null
	else
		echo "$SVEH $MKDC www.$MKDC" | sudo tee -a /etc/hosts > /dev/null
	fi  
echo -e "Edit Hosts File!\n\n"


# Edit BIND9
read -p "Enter Name: (Ex: example) " SBED1
read -p "Enter Class: (Ex: IN, Default: IN) " SBED2
SBED2=${SBED2:-IN}
read -p "Enter Type: (Ex: A, Default: A ) " SBED3
SBED3=${SBED3:-A}
read -p "Enter Server Address: (Default: 138.201.73.190) " SBED4
SBED4=${SBED4:-138.201.73.190}
echo "$SBED1 $SBED2 $SBED3 $SBED4" | sudo tee -a /etc/bind/soshaw.net.db > /dev/null
    
    # Fix BIND9 File
    #fixbind() { gawk -v a=$2 -i inplace '{for (i=1; i<=NF; i++) {printf("%s", $i);for ( i=0; i<a-length($1);i++) {printf(" ");}}print;};' $1;};
    #fixbind
echo -e "Edit BIND9 File!\n"


# Restart Services
	
	# BIND9
	sudo service bind9 restart
    echo -e "Service BIND9 Restarted!\n\n"

    # Nginx
    sudo nginx -t
    echo -e "\n"
	read -p "Do You Want to Restart Nginx Server? [Y/n] " NGRS
    if [ $NGRS == 'y' ]; then
        sudo systemctl restart nginx; echo -e "Service Nginx Restarted!" 
    else
		echo "Abort."
	fi


# Show Congratulation Message
    echo -e "\n\n Congratulation, Virtualhost Configure to Successfully! ;)"
